<?php


namespace Drupal\openid_claveunica;


use Drupal\Core\Database\Connection;
use Drupal\openid_claveunica\Plugin\OpenIDConnectClient\ClaveUnicaClientInterface;
use Drupal\user\UserInterface;

/**
 * Class AccountProfileChecker
 *
 * @package Drupal\openid_claveunica
 */
class AccountProfileChecker implements AccountProfileCheckerInterface {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * AccountProfileChecker constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function checkHashCompleteProfile(UserInterface $user, $hash): bool {
    $query = $this->database->select('users_field_data', 'u');
    $result = $query->fields('u')
      ->condition('uid', $user->id())
      ->condition($query->orConditionGroup()
        ->condition('mail', '')
        ->condition('mail', '%' . $query->escapeLike(ClaveUnicaClientInterface::TEMPORARY_SUFFIX_EMAIL), 'LIKE')
        ->isNull('mail')
      )
      ->range(0, 1)
      ->execute()
      ->fetchAllAssoc('uid');

    if (empty($result)) {
      return FALSE;
    }

    $account = array_pop($result);
    $id_hash = md5($account->name . $account->pass . $account->login);

    if ($hash != $id_hash) {
      return FALSE;
    }

    return TRUE;
  }

}
