<?php


namespace Drupal\openid_claveunica;


use Drupal\user\UserInterface;

interface AccountProfileCheckerInterface {

  /**
   * @param \Drupal\user\UserInterface $user
   * @param $hash
   *
   * @return bool
   */
  public function checkHashCompleteProfile(UserInterface $user, $hash): bool;
}
