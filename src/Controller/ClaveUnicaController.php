<?php


namespace Drupal\openid_claveunica\Controller;


use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\openid_claveunica\AccountProfileCheckerInterface;
use Drupal\openid_claveunica\Plugin\OpenIDConnectClient\ClaveUnicaClientInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ClaveUnicaController extends ControllerBase {

  /**
   * @var \Drupal\openid_claveunica\AccountProfileCheckerInterface
   */
  protected $accountProfileChecker;

  /**
   * ClaveUnicaController constructor.
   *
   * @param \Drupal\openid_claveunica\AccountProfileCheckerInterface $account_profile_checker
   */
  public function __construct(AccountProfileCheckerInterface $account_profile_checker) {
    $this->accountProfileChecker = $account_profile_checker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openid_claveunica.account_profile_checker')
    );
  }

  /**
   * The open id claveunica request to log out.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function logout(): RedirectResponse {
    $url = Url::fromUri(ClaveUnicaClientInterface::LOGOUT_ENDPOINT, [
      'query' => [
        'redirect' => Url::fromRoute('user.logout', [], [
          'absolute' => TRUE,
          'query' => [
            'from' => 'openid-claveunica',
          ],
        ])->toString(),
      ],
    ]);

    return new RedirectResponse($url->toString());
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\user\UserInterface $user
   * @param $hash
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function completeProfileAccess(AccountInterface $account, UserInterface $user, $hash): AccessResultInterface {

    if (!$account->isAnonymous()) {
      return AccessResult::forbidden();
    }

    if (!$this->accountProfileChecker->checkHashCompleteProfile($user, $hash)) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function logoutAccess(AccountInterface $account): AccessResultInterface {
    if ($account->isAuthenticated() && $this->existsSomeProviderEnabled()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * @return bool
   */
  protected function existsSomeProviderEnabled(): bool {
    try {
      $clients = $this->entityTypeManager()->getStorage('openid_connect_client')
        ->loadByProperties([
          'status' => TRUE,
          'plugin' => 'claveunica',
        ]);
    } catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return FALSE;
    }

    return !empty($clients);
  }
}
