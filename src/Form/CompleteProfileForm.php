<?php


namespace Drupal\openid_claveunica\Form;


use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\openid_claveunica\AccountProfileCheckerInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CompleteProfileForm
 *
 * @package Drupal\openid_claveunica\Form
 */
class CompleteProfileForm extends FormBase {

  /**
   * @var \Drupal\openid_claveunica\AccountProfileCheckerInterface
   */
  protected $accountProfileChecker;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected SessionInterface $session;

  /**
   * @var \Drupal\user\UserStorageInterface
   */
  protected UserStorageInterface $userStorage;

  /**
   * ClaveUnicaController constructor.
   *
   * @param \Drupal\openid_claveunica\AccountProfileCheckerInterface $account_profile_checker
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   * @param \Drupal\user\UserStorageInterface $user_storage
   */
  public function __construct(AccountProfileCheckerInterface $account_profile_checker, TimeInterface $time, ModuleHandlerInterface $module_handler, SessionInterface $session, UserStorageInterface $user_storage) {
    $this->accountProfileChecker = $account_profile_checker;
    $this->time = $time;
    $this->moduleHandler = $module_handler;
    $this->session = $session;
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CompleteProfileForm {
    return new static(
      $container->get('openid_claveunica.account_profile_checker'),
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('session'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openid_claveunica_complete_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL, $hash = NULL) {
    $form['helptext'] = array(
      '#markup' => $this->t('Please fill this form to complete your registration.'),
    );

    $form['mail'] = array(
      '#type' => 'email',
      '#title' => $this->t('E-mail address'),
      '#description' => $this->t('A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.'),
      '#required' => TRUE,
      '#default_value' => '',
    );

    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $user->id(),
    );
    $form['id_hash'] = array(
      '#type' => 'value',
      '#value' => $hash,
    );
    $form['actions'] = array(
      '#type' => 'actions',
    );
    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Complete'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uid = $form_state->getValue('uid');
    $account = User::load($uid);
    $id_hash = $form_state->getValue('id_hash');
    if (!$this->accountProfileChecker->checkHashCompleteProfile($account, $id_hash)) {
      $this->messenger()->addError($this->t('User ID cannot be identified.'));
      $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
      $response->send();
    }

    // Trim whitespace from mail, to prevent confusing 'e-mail not valid'
    // warnings often caused by cutting and pasting.
    $mail = trim($form_state->getValue('mail'));
    $form_state->setValue($form['mail'], $mail);

    $account->setEmail($mail);
    $violations = $account->validate();

    foreach ($violations->getEntityViolations() as $violation) {
      /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
      $form_state->setErrorByName(str_replace('.', '][', $violation->getPropertyPath()), $violation->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = User::load($form_state->getValue('uid'));
    $account->setEmail($form_state->getValue('mail'));
    $account->save();
    $account->setLastLoginTime($this->time->getRequestTime());

    $this->currentUser()->setAccount($account);

    $this->userStorage->updateLastLoginTimestamp($account);
    $this->logger('openid_claveunica')->info('%name filled missing fields.', array('%name' => $account->getAccountName()));

    $this->session->migrate();
    $this->session->set('uid', $account->id());
    $this->moduleHandler->invokeAll('user_login', [$account]);

    $form_state->setRedirectUrl(Url::fromRoute('<front>'));
  }

}
