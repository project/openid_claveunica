<?php


namespace Drupal\openid_claveunica\Plugin\OpenIDConnectClient;

/**
 * Interface ClaveUnicaClientInterface
 *
 * @package Drupal\openid_claveunica\Plugin\OpenIDConnectClient
 */
interface ClaveUnicaClientInterface {
  const USER_INFO_RUN_KEY = 'RolUnico';

  const USER_INFO_RUN_KEY_NUMBER = 'numero';

  const USER_INFO_RUN_KEY_DV = 'DV';

  const USER_INFO_NAME_KEY_FIRST_NAME= 'nombres';

  const USER_INFO_NAME_KEY_LAST_NAME= 'apellidos';

  const LOGOUT_ENDPOINT = 'https://accounts.claveunica.gob.cl/api/v1/accounts/app/logout';

  const AUTHORIZATION_ENDPOINT = 'https://accounts.claveunica.gob.cl/openid/authorize/';

  const TOKEN_ENDPOINT = 'https://accounts.claveunica.gob.cl/openid/token/';

  const USERINFO_ENDPOINT = 'https://accounts.claveunica.gob.cl/openid/userinfo/';

  const TEMPORARY_SUFFIX_EMAIL = '@claveunica-tmp.cl';
}
