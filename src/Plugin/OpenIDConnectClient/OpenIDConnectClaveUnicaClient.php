<?php


namespace Drupal\openid_claveunica\Plugin\OpenIDConnectClient;


use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OpenIDConnectClaveUnicaClient
 *
 * @package Drupal\openid_claveunica\Plugin\OpenIDConnectClient
 *
 * @OpenIDConnectClient(
 *   id = "claveunica",
 *   label = @Translation("Clave única")
 * )
 */
class OpenIDConnectClaveUnicaClient extends OpenIDConnectClientBase implements ClaveUnicaClientInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'label_button' => $this->t('Log in'),
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['label_button'] = [
      '#title' => $this->t('Label button'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['label_button'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array {
    return [
      'authorization' => self::AUTHORIZATION_ENDPOINT,
      'token' => self::TOKEN_ENDPOINT,
      'userinfo' => self::USERINFO_ENDPOINT,
      'end_session' => self::LOGOUT_ENDPOINT,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(string $scope = 'openid email', array $additional_params = []) :Response {
    return parent::authorize('openid run name');
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo(string $access_token): ?array {
    $userinfo = parent::retrieveUserInfo($access_token);

    if (!empty($userinfo)) {
      $userinfo['email'] = $this->generateTemporaryEmail($userinfo);
      $userinfo['preferred_username'] = $this->generatePreferredUsername($userinfo);
      $userinfo['family_name'] = $this->buildFamilyName($userinfo);
      $userinfo['given_name'] = $this->buildGivenName($userinfo);
      $userinfo['surname'] = $this->buildSurname($userinfo);
      $userinfo['second_surname'] = $this->buildSecondSurname($userinfo);
      $userinfo['run_complete'] = $this->buildRunComplete($userinfo);
      $userinfo['run_number'] = $this->buildRunNumber($userinfo);
      $userinfo['run_dv'] = $this->buildRunDv($userinfo);
      $userinfo['name'] = $this->buildFullName($userinfo);

      \Drupal::moduleHandler()->alter('openid_claveunica_post_retrieve_userinfo', $userinfo);
    }

    return $userinfo;
  }

  /**
   * Generates a temporary email to be modified later by the user.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The temporary email.
   */
  protected function generateTemporaryEmail(array $userinfo): string {
    $run = $userinfo[self::USER_INFO_RUN_KEY][self::USER_INFO_RUN_KEY_NUMBER];
    return $run . self::TEMPORARY_SUFFIX_EMAIL;
  }

  /**
   * Generate the preferred username using the run.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The preferred username.
   */
  protected function generatePreferredUsername(array $userinfo): string {
    return $userinfo[self::USER_INFO_RUN_KEY][self::USER_INFO_RUN_KEY_NUMBER];
  }

  /**
   * Build the surname.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The surname.
   */
  protected function buildSurname(array $userinfo): string {
    return $userinfo['name'][self::USER_INFO_NAME_KEY_LAST_NAME][0] ?? '';
  }

  /**
   * Build the second surname.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The second surname.
   */
  protected function buildSecondSurname(array $userinfo): string {
    return $userinfo['name'][self::USER_INFO_NAME_KEY_LAST_NAME][1] ?? '';
  }

  /**
   * Build the family name.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The family name.
   */
  protected function buildFamilyName(array $userinfo): string {
    return implode(' ', $userinfo['name'][self::USER_INFO_NAME_KEY_LAST_NAME]);
  }

  /**
   * Build the given name.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The given name.
   */
  protected function buildGivenName(array $userinfo): string {
    return implode(' ', $userinfo['name'][self::USER_INFO_NAME_KEY_FIRST_NAME]);
  }

  /**
   * Build the full name from given and family names.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The full name.
   */
  protected function buildFullName(array $userinfo): string {
    return implode(' ', array($userinfo['given_name'], $userinfo['family_name']));
  }

  /**
   * Build the run complete.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The full run.
   */
  protected function buildRunComplete(array $userinfo): string {
    return implode('-', array(
      $this->buildRunNumber($userinfo),
      $this->buildRunDv($userinfo),
    ));
  }

  /**
   * Extracts the <Run> number from userinfo.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   * @return string
   *  The run number.
   */
  protected function buildRunNumber(array $userinfo): string {
    return $userinfo[self::USER_INFO_RUN_KEY][self::USER_INFO_RUN_KEY_NUMBER];
  }

  /**
   * Extracts the check digit from the <Run>.
   *
   * @param array $userinfo
   *  The data retrieved from the provider.
   *
   * @return string
   *  The check digit.
   */
  protected function buildRunDv(array $userinfo): string {
    return $userinfo[self::USER_INFO_RUN_KEY][self::USER_INFO_RUN_KEY_DV];
  }
}
